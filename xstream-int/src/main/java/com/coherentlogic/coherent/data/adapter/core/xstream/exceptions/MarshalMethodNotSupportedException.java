package com.coherentlogic.coherent.data.adapter.core.xstream.exceptions;

import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;

/**
 * An exception that is thrown when an unsupported implementation of the {@link Converter}'s <i>marshal</i> method is
 * invoked.
 */
public class MarshalMethodNotSupportedException extends MethodNotSupportedException {

    private static final long serialVersionUID = 6709390122629840911L;

    static final String MESSAGE = "The marshal method is not supported.";

    public MarshalMethodNotSupportedException() {
        super (MESSAGE);
    }

    public MarshalMethodNotSupportedException(Throwable cause) {
        super(MESSAGE, cause);
    }
}
