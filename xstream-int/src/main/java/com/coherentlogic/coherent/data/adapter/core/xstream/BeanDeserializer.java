package com.coherentlogic.coherent.data.adapter.core.xstream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

/**
 * Used to convert XML or JSON into an instance of a domain class. This can be helpful, for example, when the
 * application has the XMl and just wants to convert it into an instance of the domain model, skipping the
 * {@link QueryBuilder}.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class BeanDeserializer {

    private static final Logger log = LoggerFactory.getLogger(BeanDeserializer.class);

    private final XStream xstream;

    public BeanDeserializer(XStream xstream) {
        this.xstream = xstream;
    }

    public <R> R deserialize (String xmlOrJSON) {

        log.debug("deserialize: method invoked; xmlOrJSON: " + xmlOrJSON); 

        return (R) xstream.fromXML(xmlOrJSON);
    }
}
