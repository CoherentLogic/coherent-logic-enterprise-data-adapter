package com.coherentlogic.coherent.data.adapter.aop.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * Test aspect which is is applied to the {@link Foo} class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Aspect
public class DefaultPropertyChangeGeneratorAspect extends AbstractPropertyChangeGenerator {

    @Around("execution(* com.coherentlogic.coherent.data.adapter.aop.domain.Foo.setBar(..))")
    @Override
    public void onSetterMethodCalled(ProceedingJoinPoint joinPoint) {
        super.onSetterMethodCalled(joinPoint);
    }
}
