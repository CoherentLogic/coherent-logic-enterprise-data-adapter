package com.coherentlogic.coherent.data.adapter.aop.aspects;

import java.beans.PropertyChangeEvent;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.aop.exceptions.PropertyChangeGeneratorException;
import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.exceptions.MisconfiguredException;

/**
 * Base class that is used for intercepting setter method calls from either AspectJ (where code is augmented
 * outside of the Spring container), or for use within the Spring container, using Spring AOP.
 *
 * PropertyChangeGenerator implementation that uses the AspectJ ProceedingJoinPoint in order to execute the
 * property change event notification in the {@link AbstractPropertyChangeGenerator}. Classes that extend
 * this class must be annotated as an Aspect and also include the specific advice on the
 * {@link #onSetterMethodCalled(ProceedingJoinPoint)}.
 *
 * Note that the org.codehaus.mojo:aspectj-maven-plugin plugin must be added to the Maven pom, similar to
 * what's included below.
 *
 *  <!--
 *    When importing this project into Eclipse you may encounter a long list of problems that looks like what
 *    appears below. The solution to this is to install the AJDT plugin from the update site below:
 *
 *    http://download.eclipse.org/tools/ajdt/46/dev/update
 *
 *    An error occurred while collecting items to be installed
 *    session context was:(profile=epp.package.dsl, phase=org.eclipse.equinox.internal.p2.engine.phases.Collect, operand=, action=).
 *    No repository found containing: osgi.bundle,org.apache.commons.io,2.4.0
 *    ...
 *
 *    See also:
 *
 *    http://atlas.apache.org/EclipseSetup.html
 *
 *    Note also that this problem may only be present when using a Mac.
 *  -->
 *
 * <build>
 *     <plugins>
 *         <plugin>
 *             <groupId>org.codehaus.mojo</groupId>
 *             <artifactId>aspectj-maven-plugin</artifactId>
 *             <version>1.10</version>
 *             <configuration>
 *                 <complianceLevel>1.8</complianceLevel>
 *                 <source>1.8</source>
 *                 <target>1.8</target>
 *                 <includes>
 *                     <include>...</include>
 *                 </includes>
 *                 <!-- TPF: Do not remove this.
 *                           https://bugs.eclipse.org/bugs/show_bug.cgi?id=490315
 *                   -->
 *                 <XnoInline>true</XnoInline>
 *             </configuration>
 *             <executions>
 *                 <execution>
 *                     <goals>
 *                         <goal>compile</goal>
 *                         <goal>test-compile</goal>
 *                     </goals>
 *                 </execution>
 *             </executions>
 *         </plugin>
 *     </plugins>
 * </build>
 *
 * @TODO Move this class into src and force developers to extend from this.
 *
 * @see {@link DefaultPropertyChangeGeneratorAspect}
 * @see /src/test/java/com.coherentlogic.coherent.data.adapter.aop.aspects.PropertyChangeGeneratorAspect
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public abstract class AbstractPropertyChangeGenerator {

    private static final Logger log = LoggerFactory.getLogger(AbstractPropertyChangeGenerator.class);

    static final String FIRE_PROPERTY_CHANGE = "firePropertyChange";

    /**
     * @see {@link SerializableBean#firePropertyChange}
     */
    private final Method firePropertyChangeMethod;

    public AbstractPropertyChangeGenerator() {

        try {
            firePropertyChangeMethod = SerializableBean.class.getDeclaredMethod(
                FIRE_PROPERTY_CHANGE, PropertyChangeEvent.class);
        } catch (Exception e) {
            throw new RuntimeException ("An exception was thrown when attempting to get the method named " +
                FIRE_PROPERTY_CHANGE + ".", e);
        }
    }

    /*
     * See if you can use method overloading instead of a closure.
     *
     * Object result = joinPoint.proceed ()
     *
     * or
     *
     * Object result = invocation.proceed();
     */
    public Object invoke(
        Object targetObject,
        Method method,
        ProceedClosure<Object> invocationOrJoinPointWrapper
    ) throws Throwable {

        SerializableBean target = asSerializableBean (targetObject);

        int parameterCount = method.getParameterCount();

        Object result = null;

        if (parameterCount == 1) { // Single parameter setter methods only.

            Parameter[] parameters = method.getParameters();

            Parameter parameter = parameters[0];

            if (parameter.isAnnotationPresent(Changeable.class)) {

                Changeable changeable = parameter.getAnnotation(Changeable.class);

                String parameterName = changeable.value();

                if (Changeable.BLANK.equals(parameterName))
                    throw new MisconfiguredException (
                        "The changeable.value (parameterName) cannot be blank.");

                Class<?> targetClass = target.getClass();

                Field field = targetClass.getDeclaredField(parameterName);

                log.debug("parameterName: " + parameterName + ", field: " + field.getName());

                doSetAndFireUpdate (
                    target,
                    field,
                    targetObject,
                    parameterName,
                    invocationOrJoinPointWrapper
                 );

            } else {
                log.debug("The method " + method + " exists and has one parameter however the parameter "
                    + "has not been annotated with the " + Changeable.class + " annotation, hence no"
                    + "PropertyChangeEvents will be fired unless the code has been manually added to "
                    + "the method.");

                result = invocationOrJoinPointWrapper.proceed();
            }
        } else {
            log.debug("The method " + method + " exists and has either zero or more than one parameter "
                + "so no PropertyChangeEvents will be fired unless the code has been manually added to "
                + "the method.");

            result = invocationOrJoinPointWrapper.proceed();
        }
        return result;
    }

    SerializableBean asSerializableBean (Object targetObject) {

        SerializableBean result = null;

        if (targetObject != null && targetObject instanceof SerializableBean)
            result = (SerializableBean) targetObject;
        else if (targetObject == null)
            throw new NullPointerException("The targetObject is null.");
        else
            throw new MisconfiguredException("The bean must extend SerializableBean; targetObject class: " +
                targetObject.getClass());

        return result;
    }

    Object doSetAndFireUpdate (
        SerializableBean target,
        Field field,
        Object targetObject,
        String parameterName,
        ProceedClosure<Object> invocationOrJoinPointWrapper
    ) throws Throwable {

        field.setAccessible(true);

        Object oldValue = field.get(targetObject);

        Object result = invocationOrJoinPointWrapper.proceed();

        Object newValue = field.get(targetObject);

        log.debug("oldValue: " + oldValue + ", newValue: " + newValue);

        field.setAccessible(false);

        PropertyChangeEvent propertyChangeEvent = new PropertyChangeEvent(
            target,
            parameterName,
            oldValue,
            newValue
        );

        firePropertyChangeMethod.setAccessible(true);

        firePropertyChangeMethod.invoke(target, propertyChangeEvent);

        firePropertyChangeMethod.setAccessible(false);

        return result;
    }

    public void onSetterMethodCalled (ProceedingJoinPoint joinPoint) {

        log.debug("onSetterMethodCalled: method begins; joinPoint: " + joinPoint);

        Object targetObject = joinPoint.getTarget();

        Class<?> clazz = targetObject.getClass();

        CodeSignature codeSignature = (CodeSignature) joinPoint.getSignature();

        Method method = null;

        if (codeSignature instanceof MethodSignature) {

            String name = codeSignature.getName();

            Class<?>[] parameterTypes = codeSignature.getParameterTypes();

            try {
                method = clazz.getMethod(name, parameterTypes);
            } catch (NoSuchMethodException | SecurityException e) {
                throw new PropertyChangeGeneratorException("Unable to get the method using the name: " + name
                    + " and parameterTypes: " + parameterTypes + " on the clazz: " + clazz);
            }

        } else
            throw new PropertyChangeGeneratorException (
                "codeSignature is not an instanceof MethodSignature; codeSignature: " + codeSignature);

        try {
            invoke(targetObject, method, () -> { return joinPoint.proceed(); });
        } catch (Throwable thrown) {
            throw new PropertyChangeGeneratorException ("The call to super.invoke resulted in an exception "
                + "being thrown; targetObject: " + targetObject + ", method: " + method, thrown);
        }

        log.debug("onSetterMethodCalled: method ends.");
    }
}
