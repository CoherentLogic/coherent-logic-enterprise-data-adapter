package com.coherentlogic.coherent.data.adapter.core.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.InvalidConfigurationException;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

public class BeanTraversalServiceTest {

    private static final Logger log = LoggerFactory.getLogger(BeanTraversalServiceTest.class);

    private BeanTraversalService beanTraversalService;

    @BeforeEach
    public void setUp () {

        beanTraversalService = new BeanTraversalService ();

        beanTraversalService.register(ExampleA.class, ExampleB.class, ExampleC.class);
    }

    @AfterEach
    public void tearDown () {
        this.beanTraversalService = null;
    }

    @Test
    public void testRegisterClassOfQArray() {
        assertThrows (
            InvalidConfigurationException.class,
            () -> {
                beanTraversalService.register(String.class);
            }
        );
    }

    @Test
    public void testForEachFieldPerBeanBiConsumerOfSerializableBeanOfQFieldSerializableBeanOfQArray() {

        ExampleA exA = new ExampleA ();

        AtomicInteger ctr = new AtomicInteger ();

        beanTraversalService.forEach(
            (SerializableBean serializableBean, Field field) -> {
                ctr.incrementAndGet();
                log.info("field.name: " + field.getName());
            },
            exA
        );

        assertEquals (3, ctr.get());
    }

    @Test
    public void testForAllFieldsPerBeanBiConsumerOfSerializableBeanOfQSetOfFieldSerializableBeanOfQArray() {

        ExampleB exB = new ExampleB ();

        AtomicInteger ctr = new AtomicInteger ();

        beanTraversalService.forEach(
            (SerializableBean serializableBean, Field field) -> {
                ctr.incrementAndGet();
                log.info("field.name: " + field.getName());
            },
            exB
        );

        assertEquals (3, ctr.get());
    }

    @Test
    public void testForAllForExampleD() {

        beanTraversalService.register(ExampleD.class);

        ExampleD exD = new ExampleD ();

        AtomicInteger ctr = new AtomicInteger ();

        beanTraversalService.forEach(
            (SerializableBean serializableBean, Field field) -> {
                ctr.incrementAndGet();
            },
            exD
        );

        assertEquals (3, ctr.get());
    }
}

class ExampleA extends SerializableBean {

    private static final long serialVersionUID = 2388390517301157923L;

    @Visitable private List<ExampleB> exampleBList = new ArrayList<ExampleB> ();

    @Visitable private final ExampleD exampleD1 = new ExampleD ();
    @Visitable private final ExampleD exampleD2 = new ExampleD ();

    // Should not be scanned.
    private final BigDecimal someOtherBigDecimalValue = new BigDecimal ("456.789");

//    public ExampleA () {
//        exampleBList.add(new ExampleB ());
//    }
}

class ExampleB extends SerializableBean {

    private static final long serialVersionUID = 3682039016464637113L;

    @Visitable private List<ExampleC> exampleCList = new ArrayList<ExampleC> ();

    @Visitable private final BigDecimal someBigDecimalValue = new BigDecimal ("123.456");

    // Should not be scanned.
    private final BigDecimal someOtherBigDecimalValue = new BigDecimal ("456.789");

    // Arrays at this time will not be inspected so we'll see this field appear once but the contents will not be
    // inspected.
    @Visitable private ExampleC[] exampleCArray = new ExampleC[] {
        new ExampleC (),
        new ExampleC ()
    };
}

class ExampleC extends SerializableBean {

    private static final long serialVersionUID = 5652239611453520550L;

    @Visitable private final BigDecimal someBigDecimalValue = new BigDecimal ("123.456");

    // Should not be scanned.
    private final BigDecimal someOtherBigDecimalValue = new BigDecimal ("456.789");
}

class ExampleD extends ExampleB {

    private static final long serialVersionUID = -5741782824749523387L;
}
