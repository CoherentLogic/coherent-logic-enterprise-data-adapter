package com.coherentlogic.coherent.data.adapter.core.builders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Consumer;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.support.GenericApplicationContext;

import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.DefaultCommandExecutor;
import com.coherentlogic.coherent.data.adapter.core.exceptions.InvalidConfigurationException;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent.EventType;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEventListener;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderExceptionEvent;
import com.coherentlogic.coherent.data.adapter.core.plugins.PluginSpecification;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * The foundation for QueryBuilder classes which the developer can use to register {@link QueryBuilderEventListener}
 * instances.
 *
 * @param <R> The result type, which is deprecated as I'm not sure we need this.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public abstract class AbstractQueryBuilder<K> {

    private static final Logger log = LoggerFactory.getLogger(AbstractQueryBuilder.class);

    // http://patorjk.com/software/taag/#p=display&h=2&v=2&f=Slant&t=Coherent%20Logic
    static final String[] WELCOME_MESSAGE = {
        "                                                                               ",
        "    ______      __                         __     __               _           ",
        "   / ____/___  / /_  ___  ________  ____  / /_   / /  ____  ____ _(_)____      " ,
        "  / /   / __ \\/ __ \\/ _ \\/ ___/ _ \\/ __ \\/ __/  / /  / __ \\/ __ `/ / ___/",
        " / /___/ /_/ / / / /  __/ /  /  __/ / / / /_   / /__/ /_/ / /_/ / / /__        ",
        " \\____/\\____/_/ /_/\\___/_/   \\___/_/ /_/\\__/  /_____|____/\\__, /_/\\___/ ",
        "                                                         /____/                ",
        " Enterprise Data Adapter Client version 3.0.3-SNAPSHOT                         ",
        "                                                                               ",
        " Brought to you by: https://coherentlogic.com                                  ",
        "                                                                               ",
        " If you need integration with additional data sources or new features then you ",
        " need to be speaking with us -- email support@coherentlogic.com to start the   ",
        " conversation.                                                                 ",
        "                                                                               "
    };

    static {
        new WelcomeMessage().addText(WELCOME_MESSAGE).display();
    }

    private final List<QueryBuilderEventListener<K>> queryBuilderEventListeners;

    private final CommandExecutorSpecification<K> commandExecutor;

    private final GenericApplicationContext applicationContext;

    public AbstractQueryBuilder() {
        this (
            new ArrayList<QueryBuilderEventListener<K>> (),
            new DefaultCommandExecutor<K> ()
        );
    }

    public AbstractQueryBuilder(
        List<QueryBuilderEventListener<K>> queryBuilderEventListeners,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        this(queryBuilderEventListeners, commandExecutor, new GenericApplicationContext ());
    }

    public AbstractQueryBuilder(
        List<QueryBuilderEventListener<K>> queryBuilderEventListeners,
        CommandExecutorSpecification<K> commandExecutor,
        GenericApplicationContext applicationContext
    ) {
        this.queryBuilderEventListeners = queryBuilderEventListeners;
        this.commandExecutor = commandExecutor;
        this.applicationContext = applicationContext;
    }

    protected GenericApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public <T extends AbstractQueryBuilder<K>> T register (PluginSpecification... pluginSpecifications) {
        return register (Arrays.asList(pluginSpecifications));
    }

    public <T extends AbstractQueryBuilder<K>> T register (List<PluginSpecification> pluginSpecifications) {

        for (PluginSpecification pluginSpecification : pluginSpecifications) {

            String pluginName = pluginSpecification.getPluginName().get();

            BeanDefinition beanDefinition = pluginSpecification.getBeanDefinition().get();

            applicationContext.registerBeanDefinition(pluginName, beanDefinition);
        }

        applicationContext.refresh();

        return (T) this;
    }

    protected CommandExecutorSpecification<K> getCommandExecutor() {
        return commandExecutor;
    }

    public <X extends AbstractQueryBuilder<K>, Y> Future<Y> invoke (Function<Map<String, Object>, Y> function) {

        Map<String, Object> context = new HashMap<String, Object> ();

        context.put("queryBuilder", this);
        context.put("applicationContext", getApplicationContext ());

        return commandExecutor.invoke(context, function);
    }

    public List<QueryBuilderEventListener<K>> getQueryBuilderListeners() {
        return queryBuilderEventListeners;
    }

    @SafeVarargs
    public final <T extends AbstractQueryBuilder<K>> T addQueryBuilderEventListener (QueryBuilderEventListener<K>... queryBuilderListeners) {

        this.queryBuilderEventListeners.addAll(Arrays.asList(queryBuilderListeners));

        return (T) this;
    }

    @SafeVarargs
    public final <T extends AbstractQueryBuilder<K>> T removeQueryBuilderEventListener (QueryBuilderEventListener<K>... queryBuilderListeners) {

        this.queryBuilderEventListeners.removeAll(Arrays.asList(queryBuilderListeners));

        return (T) this;
    }

    protected <T extends SerializableBean> void fireQueryBuilderEvent (QueryBuilderEvent<K, T> queryBuilderEvent) {
        for (QueryBuilderEventListener<K> queryBuilderListener : queryBuilderEventListeners) {
            queryBuilderListener.onEvent(queryBuilderEvent);
        }
    }

    protected <T extends SerializableBean> void fireQueryBuilderEvent (
        QueryBuilderEvent.EventType eventType,
        K key,
        T value,
        long operationBeganAtMillis,
        long operationAtThisStepMillis
    ) {
        fireQueryBuilderEvent (
            new QueryBuilderEvent<K, T> (
                this,
                eventType,
                key,
                value,
                operationBeganAtMillis,
                operationAtThisStepMillis
            )
        );
    }

    protected <T extends SerializableBean> void fireQueryBuilderEvent (
        QueryBuilderEvent.EventType eventType,
        K key,
        T value,
        Throwable cause,
        long operationBeganAtMillis,
        long operationAtThisStepMillis
    ) {
        fireQueryBuilderEvent (
            new QueryBuilderExceptionEvent<K, T> (
                this,
                eventType,
                key,
                value,
                cause,
                operationBeganAtMillis,
                operationAtThisStepMillis
            )
        );
    }

    /**
     * 
     * @param eventType
     * @param key
     * @param value
     * @param operationBeganAtMillis
     * @param operationAtThisStepMillis Set automatically in this method.
     */
    protected <T extends SerializableBean> void fireQueryBuilderEvent (
        QueryBuilderEvent.EventType eventType,
        K key,
        T value,
        long operationBeganAtMillis
    ) {
        fireQueryBuilderEvent (
            eventType,
            key,
            value,
            operationBeganAtMillis,
            System.currentTimeMillis()
        );
    }

    /**
     * 
     * @param eventType
     * @param key
     * @param value
     * @param operationBeganAtMillis
     * @param operationAtThisStepMillis Set automatically in this method.
     */
    protected <T extends SerializableBean> void fireQueryBuilderEvent (
        K key,
        T value,
        Throwable cause,
        long operationBeganAtMillis
    ) {
        fireQueryBuilderEvent (
            QueryBuilderEvent.EventType.exceptionThrown,
            key,
            value,
            cause,
            operationBeganAtMillis,
            System.currentTimeMillis()
        );
    }

    /**
     * @see #accept(Collection, Collection)
     */
    public void accept (
        Consumer<SerializableBean> visitor,
        SerializableBean... serializableBeans
    ) {
        accept (Arrays.asList(visitor), Arrays.asList(serializableBeans));
    }

    /**
     * @see #accept(Collection, Collection)
     */
    public void accept (
        Consumer<SerializableBean> visitorA,
        Consumer<SerializableBean> visitorB,
        SerializableBean... serializableBeans
    ) {
        accept (Arrays.asList(visitorA, visitorB), Arrays.asList(serializableBeans));
    }

    /**
     * @see #accept(Collection, Collection)
     *
     * @todo use AcceptorSpecification<SerializableBean> instead of SerializableBean.
     */
    public void accept (
        Consumer<SerializableBean>[] visitors,
        SerializableBean... serializableBeans
    ) {
        accept (Arrays.asList(visitors), Arrays.asList(serializableBeans));
    }

    /**
     * Method applies each of the <i>visitors</i> to each of the <i>serializableBeans</i>.
     * <p>
     * Note that the other <i>accept</i> methods delegate to this one so if it's necessary to change the default
     * behavior then override this method.
     * <p>
     * In the example below, which is borrowed from the <a href="http://bit.ly/2cIyKUA">FRED Client API</a>, the
     * {@link #accept(Consumer, SerializableBean...)} method is invoked on the categories returned from the call to the
     * FRED web services. Note that we could have invoked the accept method directly on the categories object as it
     * exists there as well.
     * <p>
     * <pre>
     * builder
     *     .category()
     *     .withApiKey(API_KEY)
     *     .withCategoryId(categoryId)
     *     .doGetAsCategories(
     *         categories -> {
     *
     *             builder.accept(
     *                 value -> {
     *                     System.out.println("value: " + value);
     *                 },
     *                 categories
     *             );
     *
     *             return categories;
     *         }
     *     );</pre>
     *
     * @param visitors The visitors will be passed to the {@link SerializableBean#accept(Collection)} method.
     *
     * @param serializableBeans The collection of {@link SerializableBean} instances that will be visited by each
     *  visitor.
     *
     * @todo Consider adding the following capabilities:
     *  pre depth first     visit the node prior to traversing its children.
     *  post depth first    visit the node after traversing its children.
     *  depth first around  visit the node and it's up to the node if/when to traverse its children.
     *  breadth first       visit the nodes in breadth-first ordering.
     *
     * @todo use AcceptorSpecification<SerializableBean> instead of SerializableBean.
     */
    public void accept (
        Collection<Consumer<SerializableBean>> visitors,
        Collection<SerializableBean> serializableBeans
    ) {
        if (serializableBeans != null) {
            serializableBeans.forEach(
                (SerializableBean serializableBean) -> {
                    serializableBean.accept(visitors);
                }
            );
        }
    }

    /**
     * Method attempts to retrieve the property using System.getProperty and if the result returned is null then checks
     * for the key using System.getenv.
     *
     * @param key The key.
     *
     * @return The value assigned to the key as either a VM-argument or an environment property set in the OS.
     *
     * @throws InvalidConfigurationException when the key points to a null value.
     */
    public String getProperty (String key) {

        if (log.isDebugEnabled())
            log.debug("getProperty: method begins; key: " + key);

        String result = System.getProperty(key);

        if (result == null)
            result = getProperty (System.getenv(), key);

        if (log.isDebugEnabled())
            log.debug("getProperty: method ends; key: " + key + ", result: " + result);

        if (result == null)
            throw new InvalidConfigurationException ("The key " + key + " must point to a non-null value (start the "
                + "API with -D" + key + "=[some value] or set they key-value-pair in the OS an environment settings).");

        return result;
    }

    /**
     * Method attempts to retrieve the property using System.getProperty and if the result returned is null then checks
     * for the key using System.getenv.
     *
     * @param key The key.
     *
     * @return The value assigned to the key as either a VM-argument or an environment property set in the OS.
     *
     * @throws InvalidConfigurationException when the key points to a null value.
     */
    String getProperty (Map<String, String> env, String key) {

        if (log.isDebugEnabled())
            log.debug("getProperty: method begins; env: " + env + ", key: " + key);

        String result = env.get(key);

        if (log.isDebugEnabled())
            log.debug("getProperty: method ends; env: " + env + ", key: " + key + ", result: " + result);

        return result;
    }

    protected void onException (K key, Object value, RuntimeException cause, long operationBeganAtMillis) {

        log.error(cause.getMessage(), cause);

        fireQueryBuilderEvent(
            EventType.exceptionThrown,
            key,
            null,
            cause,
            operationBeganAtMillis,
            System.currentTimeMillis()
        );

        throw cause;
    }

    @Override
    public String toString() {
        return "AbstractQueryBuilder [queryBuilderEventListeners=" + queryBuilderEventListeners +
            ", commandExecutor=" + commandExecutor + "]";
    }

//    /**
//     * Pre get
//     * Post get
//     *
//     * Commands are executed on data or on the queryBuilder (TBD).
//     *
//     * When doGet is called, data is returned, and then the commandExecutor executes all commands on that data, for
//     * example:
//     *
//     * Apply the command to all results:
//     *
//     * addCommand ( data -> { if data instanceof Error convert and throw new RuntimeException ();} );
//     * addCommand ( data -> { data.addPropertyChangeListener (...); } );
//     * addCommand ( queryBuilder -> { qb.getEscapedURI and then purge the value from the cache } );
//     *
//     * Only apply the command to results of type Target:
//     *
//     * addCommand ( Target.class, data -> { if data instanceof Error convert and throw new RuntimeException ();} );
//     *
//     * Commands are meant to be reused and can be used for all instances of the query builder, and assigned behind the
//     * scenes, if it makes logical sense.
//     *
//     * @param commandSpecification
//     * @return
//     */
//    public AbstractQueryBuilder<K> addCommand (CommandSpecification commandSpecification) {
//
//        commandExecutor.addCommand(commandSpecification);
//
//        return this;
//    }
}
