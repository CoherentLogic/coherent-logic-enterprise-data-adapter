package com.coherentlogic.coherent.data.adapter.core.builders;

/**
 * Specification contains methods that must be implemented when the endpoint requires a key.
 */
public interface ApiKeyRequiredSpecification {

    /**
     * Method sets the API key.
     *
     * @param apiKey The API key value -- in this case this is the actual key value and <i>not</i> the constant used to
     * identify the VM argument or OS environment key/value pair in the {@link #withExternalApiKey()} and
     *  {@link #withExternalApiKey(String)} methods.
     *
     * @return An instance of type T -- used to facilitate method chaining.
     */
    <T> T withApiKey (String apiKey);

    /**
     * Method sets the API key using the value provided in either the VM arguments or the system environment variable.
     * This method should contain the constant key value and delegate the call to {@link #withExternalApiKey(String)}.
     *
     * @see {@link com.coherentlogic.coherent.data.adapter.core.builders.AbstractQueryBuilder#getProperty(String)}.
     *
     * @return An instance of type T -- used to facilitate method chaining.
     */
    <T> T withExternalApiKey();

    /**
     * Method sets the API key using the value provided in either the VM arguments or the system environment variable.
     *
     * @see {@link com.coherentlogic.coherent.data.adapter.core.builders.AbstractQueryBuilder#getProperty(String)}.
     *
     * @param key The API key, <i>not</i> the value.
     *
     * @return An instance of type T -- used to facilitate method chaining.
     */
    <T> T withExternalApiKey(String key);
}
