package com.coherentlogic.coherent.data.adapter.core.command;

import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Function;

/**
 * Specification for CommandExecutor implementations which allows for commands to be added and executed.
 * 
 * @TODO Only one implementation of this exists at the moment so we may consider removing the interface.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public interface CommandExecutorSpecification<K> {

    <R> Future<R> invoke(Map<String, Object> context, Function<Map<String, Object>, R> function);
}
