package com.coherentlogic.coherent.data.adapter.core.builders.rest;

import java.net.URI;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.builders.AbstractCacheableQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.builders.GetMethodSpecification;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheKeyAwareSpecification;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.cache.NullCache;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.exceptions.ExecutionFailedException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.IllegalValueRuntimeException;
import com.coherentlogic.coherent.data.adapter.core.exceptions.URISyntaxException;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * This class acts as the foundation for QueryBuilder implementations. This class has caching capabilities
 * where the key is the URI and the value is an instance of a domain class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public abstract class AbstractRESTQueryBuilder<K> extends AbstractCacheableQueryBuilder<K>
    implements CacheKeyAwareSpecification<K>, GetMethodSpecification {

    private static final Logger log = LoggerFactory.getLogger(AbstractRESTQueryBuilder.class);

    private final RestTemplate restTemplate;

    /*
     * @TODO Consider replacing with the Spring UriComponentsBuilder.
     */
    private final UriBuilder uriBuilder;

    static final String[] WELCOME_MESSAGE = {
        " com.coherentlogic.enterprise-data-adapter:data-adapter-core:3.0.3-SNAPSHOT    "
    };

    static {
        new WelcomeMessage().addText(WELCOME_MESSAGE).display();
    }

    /**
     * Method returns a new instance of {@link UriBuilder} created using the
     * value of the URI.
     */
    static UriBuilder newUriBuilder (String uri) {
        UriBuilder builder;

        builder = UriBuilder.fromPath(uri);

        return builder;
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        String uri
    ) {
        this (restTemplate, newUriBuilder (uri));
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        String uri,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        this (restTemplate, newUriBuilder (uri), commandExecutor);
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        this (restTemplate, newUriBuilder (uri), cache, commandExecutor);
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        UriBuilder uriBuilder
    ) {
        this (restTemplate, uriBuilder, new NullCache<K> ());
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        this (restTemplate, uriBuilder, new NullCache<K> (), commandExecutor);
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<K> cache
    ) {
        this (restTemplate, newUriBuilder (uri), cache);
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<K> cache
    ) {
        super (cache);
        this.restTemplate = restTemplate;
        this.uriBuilder = uriBuilder;
    }

    protected AbstractRESTQueryBuilder (
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super (cache, commandExecutor);
        this.restTemplate = restTemplate;
        this.uriBuilder = uriBuilder;
    }

    /**
     * Method throws a NullPointerException if either the name or value are null; if neither reference is null a message
     * will be logged at debug level that includes the name and value.
     */
    protected AbstractRESTQueryBuilder<K> assertNotNull (String name, Object value) {

        // The uriBuilder will throw an exception if the name is null. We add
        // an additional check so that an exception is thrown if the value is
        // null. The reason for this is that the parameter should not be added
        // unless there's an appropriate value.
        if (name == null || value == null)
            throw new NullPointerException ("The name and value must both be set to non-null values (name: " + name +
                ", value: " + value + ").");
        else if (log.isDebugEnabled()) {
            log.debug("Adding the parameter with name: " + name + " and value: " + value);
        }

        return this;
    }

    /**
     * A template method that allows the user to customize the {@link #uriBuilder}.
     *
     * Note that this method is currently <u>experimental</u>.
     */
    public void customizeUriBuilder (Consumer<UriBuilder> consumer) {
        consumer.accept(getUriBuilder());
    }

    /**
     * Method adds a name-value pair to the URI -- for example, assume that we're working with the URI
     * <p>
     * http://www.coherentlogic.com
     * <p>
     * calling:
     * <p>
     * addParameter("xyz", "123");
     * <p>
     * will result in:
     * <p>
     * http://www.coherentlogic.com?xyz=123
     *
     * @param name The name of the parameter.
     * @param value The parameter value.
     *
     * @throws NullPointerException If either the name or value is null.
     */
    protected AbstractRESTQueryBuilder<K> addParameter (String name, Object value) {

        assertNotNull(name, value);

        uriBuilder.queryParam(name, value.toString());

        return this;
    }

    /**
     * Method adds a name-value date pair to the URI -- for example, assume that we're working with the URI
     * <p>
     * http://www.coherentlogic.com
     * <p>
     * calling:
     * <p>
     * addParameter("xyz", "123");
     * <p>
     * will result in:
     * <p>
     * http://www.coherentlogic.com?xyz=123
     *
     * @param name The name of the parameter.
     * @param value The parameter value.
     *
     * @throws NullPointerException If either the name or value is null.
     */
    protected AbstractRESTQueryBuilder<K> addParameter (String name, DateFormat dateFormat, Date date) {

        assertNotNull(name, date);

        String value = dateFormat.format(date);

        uriBuilder.queryParam(name, value);

        return this;
    }

    /**
     * Method adds a name-value pair to the URI -- for example, assume that we're working with the URI
     * <p>
     * http://www.coherentlogic.com
     * <p>
     * calling:
     * <p>
     * addParameter("xyz", "123");
     * <p>
     * will result in:
     * <p>
     * http://www.coherentlogic.com?xyz=123
     *
     * @param name The name of the parameter.
     * @param value The parameter value.
     *
     * @throws NullPointerException If either the name or value is null.
     */
    protected AbstractRESTQueryBuilder<K> addParameter (String name, Number value) {

        assertNotNull(name, value);

        uriBuilder.queryParam(name, value.toString());

        return this;
    }

    public static final String DEFAULT_SEPARATOR = ",";

    /**
     * Method adds a name-value pair to the internal list of name-value pairs.
     *
     * @param name The name of the parameter.
     * @param value The parameter value.
     *
     * @throws NullPointerException If either the name or value is null.
     */
    protected AbstractRESTQueryBuilder<K> addParameter (String name, String... values) {
        return addParameter (name, DEFAULT_SEPARATOR, values);
    }

    /**
     * Method adds a name-value pair to the internal list of name-value pairs.
     *
     * @param name The name of the parameter.
     * @param value The parameter value.
     *
     * @throws NullPointerException If either the name or value is null.
     */
    protected AbstractRESTQueryBuilder<K> addParameter (String name, String separator, String... values) {
        return addParameter (name, separator, Arrays.asList(values));
    }

    /**
     * Method adds a name-value pair to the internal list of name-value pairs.
     *
     * @param name The name of the parameter.
     * @param value The parameter value.
     *
     * @throws NullPointerException If either the name or value is null.
     */
    protected AbstractRESTQueryBuilder<K> addParameter (String name, String separator, List<String> values) {

        assertNotNull(name, values);
        assertNotNull("separator", separator);

        String result = "";

        for (String value : values) {
            result += value + separator;
        }

        int lastSeparator = result.lastIndexOf(separator);

        result = result.substring(0, lastSeparator);

        uriBuilder.queryParam(name, result);

        return this;
    }

    protected Object notNull (String name, Object value) {

        if (value == null)
            throw new NullPointerException("The variable named " + name + " is null.");

        return value;
    }

    protected String notNull (String name, String value) {
        return notNull (name, (Object) value).toString();
    }

    protected Integer notNull (String name, Integer value) {
        return (Integer) notNull (name, (Object) value);
    }

    protected Integer notNegative (String name, Integer value) {

        int result = value.compareTo(0);

        if (result == -1)
            throw new IllegalValueRuntimeException ("The value " + value + " for the parameter named " + name +
                " cannot be negative.");

        return value;
    }

    protected Number notNull (String name, Number value) {

        if (value == null)
            throw new NullPointerException("The variabled name " + name + " is null.");

        return value;
    }

    /**
     * Method extends the {@link #uriBuilder}'s path with the path value -- ie.
     *
     * http://www.foo.bar/ becomes http://www.foo.bar/baz/.
     *
     * @param value The additional path value -- in the example above, 'baz'.
     *
     * @deprecated See {@link #path(String)}
     */
    protected AbstractRESTQueryBuilder<K> extendPathWith (String value) {

        uriBuilder.path(value);

        return this;
    }

    /**
     * Method extends the {@link #uriBuilder}'s path with the path value -- ie.
     *
     * http://www.foo.bar/ becomes http://www.foo.bar/baz/.
     *
     * @param value The additional path value -- in the example above, 'baz'.
     *
     * @deprecated See {@link #path(String)}
     */
    protected AbstractRESTQueryBuilder<K> extendPathWith (Number value) {

        uriBuilder.path(notNull ("value", value).toString());

        return this;
    }

    String replace (String originalText, int key, Object value) {
        return replace (originalText, new Integer (key).toString(), value);
    }

    /**
     * Given the originalText string "foo/${bar}/baz" and key "${bar}" and value "boo" this method will
     * return "foo/boo/baz". Note that the key *must* be written as "${bar}" and *not* "$bar".
     */
    String replace (String originalText, String key, Object value) {

        if (originalText == null || key == null || value == null)
            throw new NullPointerException("One of the following is null and all three parameters must "
                + "have non-null references: originalText: " + originalText + ", key: " + key + ", value: "
                + value);

        String newText = originalText.replace("${" + key + "}", value.toString ());

        if (originalText.equals (newText))
            throw new URISyntaxException ("The URI has not changed after replacing the key " + key +
                " with the value " + value + " -- check the format of the originalText: " + originalText + "; key: " +
                key + ", value: " + value + ", newText: " + newText);

        return newText;
    }

    public <X extends AbstractRESTQueryBuilder<K>> X extend (
        String path,
        Object[] pathValues,
        String query,
        Object... queryValues
    ) {
        return (X) replacePath (path, Arrays.asList(pathValues)).replaceQuery(query, queryValues);
    }

    /**
     * Delegate method to the {@link #uriBuilder}.path method.
     *
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param path The remaining URI, including entries written as ${#}, which correspond to the particular
     *  entry in </i>values</i>.
     *
     * @param values The array of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @see AbstractRESTQueryBuilderTest#testExpandUsingAnArray()
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X replacePath (String path, Object... values) {
        return (X) replacePath (path, Arrays.asList(values));
    }

    /**
     * Delegate method to the {@link #uriBuilder}.path method.
     *
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param path The remaining URI, including entries written as ${#}, which correspond to the particular
     *  entry in </i>values</i>.
     *
     * @param values The array of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @see AbstractRESTQueryBuilderTest#testExpandUsingAnArray()
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X replacePath (String path, List<Object> values) {

        String result = path;

        int ctr = 0;

        for (Object value : values)
            result = replace (result, ctr++, value);

        uriBuilder.replacePath(getEscapedURI() + result);

        return (X) this;
    }

    /**
     * Delegate method to the {@link #uriBuilder}.path method.
     *
     * Replaces each occurrence of ${entrySet.key} with the corresponding <i>entrySet.value</i>.
     *
     * @param path The remaining URI, including entries written as ${entrySet.key}.
     *
     * @param values The map of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X replacePath (String path, Map<String, ?> values) {

        String result = path;

        for (Entry<String, ?> entry : values.entrySet())
            result = replace (result, entry.getKey(), entry.getValue());

        uriBuilder.replacePath(getEscapedURI() + result);

        return (X) this;
    }

    /**
     * EXPERIMENTAL
     *
     * Delegate method to the {@link #uriBuilder}.fragment method.
     *
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param path The remaining URI, including entries written as ${#}, which correspond to the particular
     *  entry in </i>values</i>.
     *
     * @param values The array of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @see AbstractRESTQueryBuilderTest#testExpandUsingAnArray()
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X fragment (String path, Object... values) {
        return (X) fragment (path, Arrays.asList(values));
    }

    /**
     * EXPERIMENTAL
     *
     * Delegate method to the {@link #uriBuilder}.fragment method.
     *
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param path The remaining URI, including entries written as ${#}, which correspond to the particular
     *  entry in </i>values</i>.
     *
     * @param values The array of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @see AbstractRESTQueryBuilderTest#testExpandUsingAnArray()
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X fragment (String path, List<Object> values) {

        String result = path;

        int ctr = 0;

        for (Object value : values)
            result = replace (result, ctr++, value);

        uriBuilder.fragment(result);

        return (X) this;
    }

    /**
     * EXPERIMENTAL
     *
     * Delegate method to the {@link #uriBuilder}.fragment method.
     *
     * Replaces each occurrence of ${entrySet.key} with the corresponding <i>entrySet.value</i>.
     *
     * @param path The remaining URI, including entries written as ${entrySet.key}.
     *
     * @param values The map of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X fragment (String path, Map<String, ?> values) {

        String result = path;

        for (Entry<String, ?> entry : values.entrySet())
            result = replace (result, entry.getKey(), entry.getValue());

        uriBuilder.fragment(result);

        return (X) this;
    }

    /**
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param path The remaining URI, including entries written as ${#}, which correspond to the particular
     *  entry in </i>values</i>.
     *
     * @param values The array of values to substitute into the path.
     *
     * @return An instance of this.
     *
     * @see AbstractRESTQueryBuilderTest#testExpandUsingAnArray()
     *
     * @TODO: Move to the base class and/or add an interface.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X replaceQuery (String path, Object... values) {
        return (X) replaceQuery (path, Arrays.asList(values));
    }

    /**
     * Delegate method to the {@link #uriBuilder}.replaceQuery method.
     *
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param query The URI query, including entries written as ${#}, which correspond to the particular
     *  entry in </i>values</i>.
     *
     * @param values The array of values to substitute in the query.
     *
     * @return An instance of this.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X replaceQuery (String query, List<Object> values) {

        String result = query;

        int ctr = 0;

        for (Object value : values)
            result = replace (result, ctr++, value);

        log.debug("query: " + query + ", values: " + values + ", result: " + result);

        uriBuilder.replaceQuery(result);

        return (X) this;
    }

    /**
     * Delegate method to the {@link #uriBuilder}.replaceQuery method.
     *
     * Replaces each occurrence of ${index #} with the corresponding value in <i>values</i>.
     *
     * @param query The URI query, including entries written as ${entrySet.key}, which correspond to the
     *  particular entry in </i>values</i>.
     *
     * @param values The map of values to substitute into the path.
     *
     * @return An instance of this.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X replaceQuery (String query, Map<String, ?> values) {

        String result = query;

        for (Entry<String, ?> entry : values.entrySet())
            result = replace (result, entry.getKey(), entry.getValue());

        uriBuilder.replaceQuery(result);

        return (X) this;
    }

    /**
     * Method not yet implemented.
     */
    protected <X extends AbstractRESTQueryBuilder<K>> X withBody (String body) {
        throw new RuntimeException ("Method not yet implemented!");
    }

    /**
     * Method not yet implemented.
     */
    protected <X extends AbstractRESTQueryBuilder<K>> X withBody (String body, Object...values) {
        throw new RuntimeException ("Method not yet implemented!");
    }

    /**
     * Method not yet implemented.
     */
    protected <X extends AbstractRESTQueryBuilder<K>> X withBody (String body, Map<String, ?>...values) {
        throw new RuntimeException ("Method not yet implemented!");
    }

    /**
     * Method returns the escaped endpoint URI.
     *
     * This method can be useful when debugging, just print the escaped URI and
     * paste it into the address bar in your browser and see what's returned.
     *
     * @return A sting representation of the escaped URI.
     *
     * @todo This method needs to go in a parent class.
     */
    public String getEscapedURI () {

        URI uri = uriBuilder.build();

        String escapedURI = uri.toASCIIString();

        return escapedURI;
    }

    /**
     * Getter method for the RestTemplate.
     */
    protected RestTemplate getRestTemplate() {
        return restTemplate;
    }

    /**
     * Getter method for the RestTemplate.
     */
    protected UriBuilder getUriBuilder() {
        return uriBuilder;
    }

//    /**
//     * Method is used to get the key for this instance of the {@link AbstractRESTQueryBuilder} -- the key is
//     * then used to lookup the value in the cache. If the value exists then it is returned to the user,
//     * otherwise the call to the web service will be executed.
//     *
//     * @todo Rename this to getCacheKey and extract an interface and also use this in the AbstractCSVQueryBuilder.
//     */
//    protected abstract K getCacheKey ();

    private static final DebugInterceptor debugInterceptor = new DebugInterceptor ();

    /**
     * FOR DEVELOPMENT PURPOSES ONLY
     *
     * Adds an instance of the {@link DebugInterceptor} to the underlying restTemplate's interceptors list.
     *
     * Note that since the restTemplate is a singleton reference and is hence typically shared with all other
     * instances of this class, calling this method will add the interceptor that will log messages for *all*
     * calls made, so <b>use this for development purposes only<b>.
     */
    public <X extends AbstractRESTQueryBuilder<K>> X debug () {

        restTemplate.getInterceptors().add(debugInterceptor);

        return (X) this;
    }

    /**
     * Use the {@link #restTemplate} to execute the call to the web service -- this can be a get or post
     * operation, etc.
     */
    protected abstract <T> T doExecute (Class<T> type);

    /**
     * Method constructs the URI and first checks to see if the object currently exists in the cache -- if it
     * does, then this object is returned, otherwise the URI is called and the resultant XML is converted
     * into an instance of type <i>type</i> and the result is returned to the user.
     */
    @Override
    public <T extends SerializableBean> T doGet (Class<T> type) {

        long operationBeganAtMillis = System.currentTimeMillis();

        fireQueryBuilderEvent(QueryBuilderEvent.EventType.methodBegins, null, null, operationBeganAtMillis);

        K key = getCacheKey ();

        T result = null;

        CacheServiceProviderSpecification<K> cache = getCache();

        fireQueryBuilderEvent(
            QueryBuilderEvent.EventType.preCacheCheck,
            key,
            null,
            operationBeganAtMillis
        );

        Object cachedObject = cache.get(key);

        if (cachedObject != null && !type.isInstance(cachedObject)) {

            RuntimeException exception = new ClassCastException (
                "The cached object " + cachedObject + " cannot be cast to type " + type + ".");

            onException(key, result, exception, operationBeganAtMillis);
        }

        log.debug("Post cache get for key: " + key + " and serializableBean (result): " + cachedObject +
            " from the cache: " + cache);

        if (cachedObject != null) {

            result = (T) cachedObject;

            fireQueryBuilderEvent(
                QueryBuilderEvent.EventType.cacheHit,
                key,
                result,
                operationBeganAtMillis
            );

        }  else if (cachedObject == null) {

            try {
                result = doExecute (type);
            } catch (Throwable cause) {

                onException (
                    key,
                    null,
                    new ExecutionFailedException (
                        "The call to the doExecute method caused an exception to be thrown (escapedURI: " +
                            getEscapedURI() +")",
                        cause
                    ),
                    operationBeganAtMillis
                );
            }

            log.debug("Will add key: " + key + " and result: " + result + " to the cache: " + cache);

            cache.put(key, result);

            fireQueryBuilderEvent(
                QueryBuilderEvent.EventType.cacheMiss,
                key,
                result,
                operationBeganAtMillis
            );
        }

        fireQueryBuilderEvent(QueryBuilderEvent.EventType.methodEnds, key, result, operationBeganAtMillis);

        return result;
    }

    /**
     * Method executes the call to the URI returned from the call to {@link #getEscapedURI()} and returns the
     * result as a String.
     *
     * Note that this method does <i>not</i> cache responses so every call to this method will result in a
     * call being made to the web service end point.
     */
    public String doGetAsString() {
        return doGetAsString (String.class, (String result) -> { return result; });
    }

    /**
     * Method executes the call to the URI returned from the call to {@link #getEscapedURI()} and returns the
     * result of type R. Note that a new instance of RestTemplate each time this method is called.
     *
     * Note that this method does <i>not</i> cache responses so every call to this method will result in a
     * call being made to the web service end point.
     *
     * @param resultType The class of the result returned from the function.
     *
     * @param function Will be applied to the result and returns a result of type R.
     *
     * @return A result of type R.
     */
    public <R> R doGetAsString(Class<R> resultType, Function<String, R> function) {
        return doGetAsString (new RestTemplate (), resultType, function);
    }

    /**
     * Method executes the call to the URI returned from the call to {@link #getEscapedURI()} and returns the
     * result of type R.
     *
     * Note that this method does <i>not</i> cache responses so every call to this method will result in a
     * call being made to the web service end point.
     *
     * Developers can override this method when it's necessary to use a different method (ie. postForObject).
     *
     * @param restTemplate The instance used to execute the GET web service call (via the getForObject
     *  method).
     *
     * @param resultType The class of the result returned from the function.
     *
     * @param function Will be applied to the result and returns a result of type R.
     *
     * @return A result of type R.
     */
    protected <R> R doGetAsString(RestTemplate restTemplate, Class<R> resultType,
        Function<String, R> function) {

        return doGetAsString (
            restTemplate,
            (RestTemplate webMethodCall) -> {
                return webMethodCall.getForObject(getEscapedURI(), String.class);
            },
            resultType,
            function
        );
    }

    /**
     * Method executes the call to the URI returned from the call to {@link #getEscapedURI()} and then
     * invokes the specified function and returns the result of type R.
     *
     * Note that this method does <i>not</i> cache responses so every call to this method will result in a
     * call being made to the web service end point.
     *
     * Developers can override this method when it's necessary to use a different method (ie. postForObject).
     *
     * @param restTemplate The instance used to execute the GET web service call (via the
     *  getForObject method).
     *
     * @param webMethod A function that will execute the web method call using the restTemplate.
     *
     * @param resultType The class of the result returned from the function.
     *
     * @param function Will be applied to the result and then returns a result of type R.
     *
     * @return A result of type R.
     */
    protected <R> R doGetAsString(
        RestTemplate restTemplate,
        Function<RestTemplate, String> webMethod,
        Class<R> resultType,
        Function<String, R> function
    ) {
        log.debug("doGetAsString: method begins; restTemplate: " + restTemplate + ", webMethod: " + webMethod
            + ", resultType: " + resultType + ", function: " + function);

        String result;

        try {
            result = webMethod.apply(restTemplate);
        } catch (Exception cause) {

            String uri = getEscapedURI();

            throw new ExecutionFailedException (
                "The call to webMethod.apply resulted in an exception being thrown (uri: " + uri + ")",
                cause
            );
        }

        log.debug("doGetAsString: method ends; result: " + result);

        return function.apply(result);
    }

    @Override
    public String toString() {
        return "AbstractRESTQueryBuilder [restTemplate=" + restTemplate + ", uriBuilder=" + uriBuilder +
            ", toString()=" + super.toString() + "]";
    }
}
