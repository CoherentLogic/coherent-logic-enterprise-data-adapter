package com.coherentlogic.coherent.data.adapter.core.command;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.builders.AbstractQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;

/**
 * A command executor implementation which creates a thread pool of nThreads fixed size and which is used to execute
 * the functions passed to the {@link #invoke(AbstractQueryBuilder, Function)} method.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class MultithreadedCommandExecutor implements CommandExecutorSpecification<String> {

    private static final Logger log = LoggerFactory.getLogger(MultithreadedCommandExecutor.class);

    private final ExecutorService executorService;

    public MultithreadedCommandExecutor (int nThreads) {
        executorService = Executors.newFixedThreadPool(nThreads);
    }

	@Override
	public <R> Future<R> invoke(Map<String, Object> context, Function<Map<String, Object>, R> function) {
		
        log.debug("invoke: method begins; currentThread: " + Thread.currentThread() + ", context: " + context +
                ", function: " + function);

        Future<R> result = executorService.submit(
            new Callable<R> () {

                @Override
                public R call() throws Exception {

                    log.debug("call: method begins; currentThread: " + Thread.currentThread() + ", context: " +
                        context + ", function: " + function);

                    R result = function.apply(context);

                    log.debug("call: method ends; result: " + result);

                    return result;
                }
            }
        );

        log.debug("invoke: method ends; result: " + result);

        return result;
    }

//    @Override
//    public <X extends AbstractQueryBuilder<String>, Y> Future<Y> invoke(Map<String, Object> context, Function<X, Y> function) {
//
//        log.debug("invoke: method begins; currentThread: " + Thread.currentThread() + ", queryBuilder: " + queryBuilder +
//                ", function: " + function);
//
//        Future<Y> result = executorService.submit(
//            new Callable<Y> () {
//
//                @Override
//                public Y call() throws Exception {
//
//                    log.debug("call: method begins; currentThread: " + Thread.currentThread() + ", queryBuilder: " +
//                        queryBuilder + ", function: " + function);
//
//                    Y result = function.apply(queryBuilder);
//
//                    log.debug("call: method ends; result: " + result);
//
//                    return result;
//                }
//            }
//        );
//
//        log.debug("invoke: method ends; result: " + result);
//
//        return result;
//    }
}
