package com.coherentlogic.coherent.data.adapter.core.builders;

/**
 * Allows the user to:
 * - Add capabilities to an adapter. These capabilities, however, may or may not be needed by other adapters.
 * - Add dependencies to an adapter.
 * - Develop a common set of plugins along with optional plugins and plugins which are defined by other devs.
 *
 * Figure it this way: If we needed CredStash, for example, then we add a plugin which has the CredStash
 * dependencies along with an implementation of the Plugin interface which includes the logic to achieve what
 * we need to with CredStash -- in this case a delegate method to get a value for a given key.
 *
 * In this example we now have a JCredStashPlugin which we can rely on when we're writing other adapters.
 * We can then implement a method such as withEncryptionKey with something like:
 * 
 * withEncryptionKey (String encryptionKeyName) {
 *     encryptionKeyValue = credStashPlugin.getencryptionKeyValue (encryptionKeyName)
 * }
 *
 * Some possible plugins:
 * - Scripting (javax.script) / JSR-223
 *   See also:
 *   * https://docs.oracle.com/javase/9/scripting/scripting-languages-and-java.htm#JSJSG107
 *   * https://docs.oracle.com/javase/9/docs/api/jdk.scripting.nashorn-summary.html
 * - CredStash integration
 * 
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
public interface Plugin {
}
