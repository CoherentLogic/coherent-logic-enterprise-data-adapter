package com.coherentlogic.coherent.data.adapter.core.exceptions;

import org.springframework.core.NestedRuntimeException;

public class GenericRuntimeException extends NestedRuntimeException {

    private static final long serialVersionUID = -6649442152387449668L;

    public GenericRuntimeException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public GenericRuntimeException(String msg) {
        super(msg);
    }
}
