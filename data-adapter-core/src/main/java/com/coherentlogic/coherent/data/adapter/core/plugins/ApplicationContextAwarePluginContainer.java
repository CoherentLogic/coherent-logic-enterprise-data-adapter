package com.coherentlogic.coherent.data.adapter.core.plugins;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component//(name="applicationContextAwarePluginContainer")
public class ApplicationContextAwarePluginContainer implements PluginContainerSpecification {

    @Autowired
    private ApplicationContext applicationContext;

    public ApplicationContextAwarePluginContainer(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public <T> T lookup(String name) {
        return (T) applicationContext.getBean(name);
    }

    @Override
    public <T> T lookup(Class<?> type) {
        return (T) applicationContext.getBean(type);
    }

    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public String toString() {
        return "ApplicationContextAwarePluginContainer [applicationContext=" + applicationContext + "]";
    }
}
