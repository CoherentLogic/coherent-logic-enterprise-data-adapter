package com.coherentlogic.coherent.data.adapter.core.builders.rest.json;

import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public abstract class AbstractGSONQueryBuilder<K> extends AbstractRESTQueryBuilder<K> {

    private static final Logger log = LoggerFactory.getLogger(AbstractGSONQueryBuilder.class);

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, String uri, CacheServiceProviderSpecification<K> cache,
            CommandExecutorSpecification<K> commandExecutor) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, String uri, CacheServiceProviderSpecification<K> cache) {
        super(restTemplate, uri, cache);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, String uri,
            CommandExecutorSpecification<K> commandExecutor) {
        super(restTemplate, uri, commandExecutor);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder,
            CacheServiceProviderSpecification<K> cache, CommandExecutorSpecification<K> commandExecutor) {
        super(restTemplate, uriBuilder, cache, commandExecutor);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder,
            CacheServiceProviderSpecification<K> cache) {
        super(restTemplate, uriBuilder, cache);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder,
            CommandExecutorSpecification<K> commandExecutor) {
        super(restTemplate, uriBuilder, commandExecutor);
    }

    public AbstractGSONQueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    /**
     * Methods returns the results from the web service call as an instance of {@link JsonElement}.
     */
    public JsonElement doGetAsJsonElement () {
        return doGetAsJsonElement ((JsonElement jsonElement) -> { return jsonElement; });
    }

    /**
     * Methods passes the results from the web service call as an instance of {@link JsonElement} and to the specified
     * function and then returns that result as type R.
     */
    public <R> R doGetAsJsonElement (Function<JsonElement, R> function) {

        String json = doGetAsString();

        return doGetAsJsonElement (json, function);
    }

    /**
     * Methods passes the results from the web service call as an instance of {@link JsonElement} and to the specified
     * function and then returns that result as type R.
     */
    public <R> R doGetAsJsonElement (String json, Function<JsonElement, R> function) {

        if (log.isDebugEnabled())
            log.debug("json: " + json);

        JsonParser parser = new JsonParser();

        JsonElement result = parser.parse(json).getAsJsonObject();

        return function.apply(result);
    }
}
