package com.coherentlogic.coherent.data.adapter.core.repositories.security;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.coherentlogic.coherent.data.model.core.domain.security.GrantedAuthorityBean;

@Transactional
public interface GrantedAuthorityRepository extends JpaRepository<GrantedAuthorityBean, Long> {

}
