package com.coherentlogic.coherent.data.adapter.core.services.security;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import com.coherentlogic.coherent.data.adapter.core.repositories.security.UserDetailsRepository;
import com.coherentlogic.coherent.data.model.core.domain.security.UserDetailsBean;
import com.jamonapi.Monitor;
import com.jamonapi.MonitorFactory;

/**
 * 
@Repository(RoleService.BEAN_NAME)
@Transactional
public class RoleService {

    public static final String BEAN_NAME = "roleService";
 *
 */
@Repository(UserDetailsService.BEAN_NAME)
@Transactional
public class UserDetailsService
    implements org.springframework.security.core.userdetails.UserDetailsService {

    private static final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

    static final String BEAN_NAME = "userDetailsService";

    @Autowired
    private UserDetailsRepository userDetailsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Monitor monitor = MonitorFactory.start("loadUserByUsername method");

        log.info("loadUserByUsername: method begins; userName: " + username);

        UserDetailsBean result = findByUsername(username);

        String stats = monitor.stop().toString();

        log.info("loadUserByUsername: method ends; result: " + result + "stats: " + stats);

        return result;
    }

    public long count() {
        return userDetailsRepository.count();
    }

    public <S extends UserDetailsBean> long count(Example<S> arg0) {
        return userDetailsRepository.count(arg0);
    }

    public void delete(Long arg0) {
        userDetailsRepository.deleteById(arg0);
    }

    public void delete(UserDetailsBean arg0) {
        userDetailsRepository.delete(arg0);
    }

    public void delete(Iterable<? extends UserDetailsBean> arg0) {
        userDetailsRepository.deleteAll(arg0);
    }

    public void deleteAll() {
        userDetailsRepository.deleteAll();
    }

    public void deleteAllInBatch() {
        userDetailsRepository.deleteAllInBatch();
    }

    public void deleteInBatch(Iterable<UserDetailsBean> arg0) {
        userDetailsRepository.deleteInBatch(arg0);
    }

    public boolean exists(Long arg0) {
        return userDetailsRepository.existsById(arg0);
    }

    public <S extends UserDetailsBean> boolean exists(Example<S> arg0) {
        return userDetailsRepository.exists(arg0);
    }

    public List<UserDetailsBean> findAll() {
        return userDetailsRepository.findAll();
    }

    public List<UserDetailsBean> findAll(Sort arg0) {
        return userDetailsRepository.findAll(arg0);
    }

    public List<UserDetailsBean> findAll(Iterable<Long> arg0) {
        return userDetailsRepository.findAllById(arg0);
    }

    public <S extends UserDetailsBean> List<S> findAll(Example<S> arg0) {
        return userDetailsRepository.findAll(arg0);
    }

    public <S extends UserDetailsBean> List<S> findAll(Example<S> arg0, Sort arg1) {
        return userDetailsRepository.findAll(arg0, arg1);
    }

    public Page<UserDetailsBean> findAll(Pageable arg0) {
        return userDetailsRepository.findAll(arg0);
    }

    public <S extends UserDetailsBean> Page<S> findAll(Example<S> arg0, Pageable arg1) {
        return userDetailsRepository.findAll(arg0, arg1);
    }

    public UserDetailsBean findByUsername(String username) {
        return userDetailsRepository.findByUsername(username);
    }

    public UserDetailsBean findOne(Long arg0) {
        return userDetailsRepository.getOne(arg0);
    }

    public <S extends UserDetailsBean> S findOne(Example<S> arg0) {
        // This needs to be tested, note use of get.
        return userDetailsRepository.findOne(arg0).get();
    }

    public void flush() {
        userDetailsRepository.flush();
    }

    public UserDetailsBean getOne(Long arg0) {
        return userDetailsRepository.getOne(arg0);
    }

    public <S extends UserDetailsBean> List<S> save(Iterable<S> arg0) {
        return userDetailsRepository.saveAll(arg0);
    }

    public <S extends UserDetailsBean> S save(S arg0) {
        return userDetailsRepository.save(arg0);
    }

    public <S extends UserDetailsBean> S saveAndFlush(S arg0) {
        return userDetailsRepository.saveAndFlush(arg0);
    }

    @Override
    public String toString() {
        return "UserDetailsService [userDetailsRepository=" + userDetailsRepository + "]";
    }
}
