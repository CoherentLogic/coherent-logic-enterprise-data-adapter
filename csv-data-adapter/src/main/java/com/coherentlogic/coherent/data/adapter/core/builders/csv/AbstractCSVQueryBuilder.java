package com.coherentlogic.coherent.data.adapter.core.builders.csv;

import java.util.List;

import org.apache.commons.csv.CSVParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.builders.AbstractCacheableQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.builders.GetMethodSpecification;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheKeyAwareSpecification;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;
import com.coherentlogic.coherent.data.adapter.core.exceptions.ExecutionFailedException;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEvent;
import com.coherentlogic.coherent.data.adapter.core.listeners.QueryBuilderEventListener;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * 
 * @author Thomas P. Fuller
 *
 * @param <K> The key type.
 */
public abstract class AbstractCSVQueryBuilder<K> extends AbstractCacheableQueryBuilder<K>
    implements CacheKeyAwareSpecification<K>, GetMethodSpecification {

    private static final Logger log = LoggerFactory.getLogger(AbstractCSVQueryBuilder.class);

    static final String[] WELCOME_MESSAGE = {
        " com.coherentlogic.enterprise-data-adapter:csv-data-adapter:3.0.3-SNAPSHOT     "
    };

    private CSVParser csvParser;

    private K cacheKey;

    public AbstractCSVQueryBuilder() {
    }

    public AbstractCSVQueryBuilder(CSVParser csvParser) {
        this.csvParser = csvParser;
    }

    public AbstractCSVQueryBuilder(
        CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super(cache, commandExecutor);
    }

    public AbstractCSVQueryBuilder(
        CacheServiceProviderSpecification<K> cache,
        CommandExecutorSpecification<K> commandExecutor,
        CSVParser csvParser
    ) {
        super(cache, commandExecutor);

        this.csvParser = csvParser;
    }

    public AbstractCSVQueryBuilder(
        CacheServiceProviderSpecification<K> cache,
        List<QueryBuilderEventListener<K>> queryBuilderListeners,
        CommandExecutorSpecification<K> commandExecutor
    ) {
        super(cache, queryBuilderListeners, commandExecutor);
    }

    public AbstractCSVQueryBuilder(
        CacheServiceProviderSpecification<K> cache,
        List<QueryBuilderEventListener<K>> queryBuilderListeners,
        CommandExecutorSpecification<K> commandExecutor,
        CSVParser csvParser
    ) {
        super(cache, queryBuilderListeners, commandExecutor);

        this.csvParser = csvParser;
    }

    public AbstractCSVQueryBuilder(CacheServiceProviderSpecification<K> cache) {

        super(cache);

        this.csvParser = csvParser;
    }

    public AbstractCSVQueryBuilder(CacheServiceProviderSpecification<K> cache, CSVParser csvParser) {

        super(cache);

        this.csvParser = csvParser;
    }

    public AbstractCSVQueryBuilder(CommandExecutorSpecification<K> commandExecutor) {
        super(commandExecutor);
    }

    public AbstractCSVQueryBuilder(CommandExecutorSpecification<K> commandExecutor, CSVParser csvParser) {

        super(commandExecutor);

        this.csvParser = csvParser;
    }

    public void setCSVParser(CSVParser csvParser) {
        this.csvParser = csvParser;
    }

    public AbstractCSVQueryBuilder<K> withCSVParser(CSVParser csvParser) {

        setCSVParser (csvParser);

        return this;
    }

    /**
     * Template method for customizing the instance of {@link CSVParser}.
     *
     * @return An instance of this for the purposes of method chaining.
     */
    public AbstractCSVQueryBuilder<K> customizeCSVParser (CSVParser csvParser) {
        return this;
    }

    public void setCacheKey (K cacheKey) {
        this.cacheKey = cacheKey;
    }

    public AbstractCSVQueryBuilder<K> withCacheKey (K k) {

        setCacheKey (cacheKey);

        return this;
    }

    public K getCacheKey () {
        return cacheKey;
    }

    protected CSVParser getCSVParser() {
        return csvParser;
    }

    /**
     * Parse record-wise:
     *
     * Call the following:
     *
     *   CSVFormat.EXCEL.parse(in)
     *
     * and pass the resultant CSVParser instance into the ctor and then execute the following line in the doExecute
     * implementation:
     *
     * for(CSVRecord record : CSVFormat.EXCEL.parse(in)) { ... }
     *
     * Parse into memory:
     *
     *   Reader in = new StringReader("some.csv");
     *   CSVParser parser = new CSVParser(in, CSVFormat.EXCEL);
     *
     * and pass the resultant CSVParser instance into the ctor and then execute the following line in the doExecute
     * implementation:
     *
     * List<CSVRecord> list = parser.getRecords();
     *
     * @see <a href="http://commons.apache.org/proper/commons-csv/apidocs/org/apache/commons/csv/CSVParser.html">
     * CSVParser Javadoc</a>
     */
    protected abstract <T> T doExecute (Class<T> type);

    /**
     * Method constructs the URI and first checks to see if the object currently exists in the cache -- if it
     * does, then this object is returned, otherwise the URI is called and the resultant XML is converted
     * into an instance of type <i>type</i> and the result is returned to the user.
     */
    @Override
    public <T extends SerializableBean> T doGet (Class<T> type) {

        long operationBeganAtMillis = System.currentTimeMillis();

        fireQueryBuilderEvent(QueryBuilderEvent.EventType.methodBegins, null, null, operationBeganAtMillis);

        K key = getCacheKey ();

        T result = null;

        CacheServiceProviderSpecification<K> cache = getCache();

        fireQueryBuilderEvent(
            QueryBuilderEvent.EventType.preCacheCheck,
            key,
            null,
            operationBeganAtMillis
        );

        Object cachedObject = cache.get(key);

        if (cachedObject != null && !type.isInstance(cachedObject)) {

            RuntimeException exception = new ClassCastException (
                "The cached object " + cachedObject + " cannot be cast to type " + type + ".");

            onException(key, result, exception, operationBeganAtMillis);
        }

        log.debug("Post cache get for key: " + key + " and serializableBean (result): " + cachedObject +
            " from the cache: " + cache);

        if (cachedObject != null) {

            result = (T) cachedObject;

            fireQueryBuilderEvent(
                QueryBuilderEvent.EventType.cacheHit,
                key,
                result,
                operationBeganAtMillis
            );

        }  else if (cachedObject == null) {

            try {
                // @TODO: We might deprecate this and then we can move this logic into the doExecute method.
                customizeCSVParser (csvParser);

                result = doExecute (type);
            } catch (Throwable cause) {

                onException (
                    key,
                    null,
                    new ExecutionFailedException (
                        "The call to the doExecute method caused an exception to be thrown.",
                        cause
                    ),
                    operationBeganAtMillis
                );
            }

            log.debug("Will add key: " + key + " and result: " + result + " to the cache: " + cache);

            cache.put(key, result);

            fireQueryBuilderEvent(
                QueryBuilderEvent.EventType.cacheMiss,
                key,
                result,
                operationBeganAtMillis
            );
        }

        fireQueryBuilderEvent(QueryBuilderEvent.EventType.methodEnds, key, result, operationBeganAtMillis);

        return result;
    }

    public <T extends SerializableBean> T aggregate (Class<T> type) {
    	return null;
    }
}
