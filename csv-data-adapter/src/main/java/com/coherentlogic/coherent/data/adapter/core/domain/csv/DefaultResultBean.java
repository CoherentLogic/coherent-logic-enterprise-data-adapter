package com.coherentlogic.coherent.data.adapter.core.domain.csv;

import java.beans.PropertyChangeSupport;
import java.beans.VetoableChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.coherent.data.model.core.listeners.AggregatePropertyChangeListener;

/**
 * A default result with headers and content; headers are, of course, all Strings and the content is also typed as a
 * String.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class DefaultResultBean extends SerializableBean {

    private static final long serialVersionUID = 7295173929569739591L;

    private List<String> headers;

    private List<List<String>> contents;

    public DefaultResultBean() {
        this (new ArrayList<String> (), new ArrayList (new ArrayList<String> ()));
    }

    public DefaultResultBean(List<String> headers, List<List<String>> content) {
        this.headers = headers;
        this.contents = content;
    }

    public DefaultResultBean(
        PropertyChangeSupport propertyChangeSupport,
        VetoableChangeSupport vetoableChangeSupport,
        List<AggregatePropertyChangeListener<SerializableBean>> aggregatePropertyChangeListeners
    ) {
        this(
            propertyChangeSupport,
            vetoableChangeSupport,
            aggregatePropertyChangeListeners,
            new ArrayList<String> (),
            new ArrayList (new ArrayList<String> ())
        );
    }

    public DefaultResultBean(
        PropertyChangeSupport propertyChangeSupport,
        VetoableChangeSupport vetoableChangeSupport,
        List<AggregatePropertyChangeListener<SerializableBean>> aggregatePropertyChangeListeners,
        List<String> headers,
        List<List<String>> content
    ) {
        super(propertyChangeSupport, vetoableChangeSupport, aggregatePropertyChangeListeners);

        this.headers = headers;
        this.contents = content;
    }

    public List<String> getHeaders() {
        return headers;
    }

    public void setHeaders(List<String> headers) {
        this.headers = headers;
    }

    public void addHeader(String header) {
        this.headers.add(header);
    }

    public Collection<? extends List<String>> getContents() {
        return contents;
    }

    public void setContents(List<List<String>> contents) {
        this.contents = contents;
    }

    /**
     * Adds a row to the contents list.
     *
     * @param content A single row.
     */
    public void addContent(List<String> content) {
        this.contents.add(content);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((contents == null) ? 0 : contents.hashCode());
        result = prime * result + ((headers == null) ? 0 : headers.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        DefaultResultBean other = (DefaultResultBean) obj;
        if (contents == null) {
            if (other.contents != null)
                return false;
        } else if (!contents.equals(other.contents))
            return false;
        if (headers == null) {
            if (other.headers != null)
                return false;
        } else if (!headers.equals(other.headers))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DefaultResultBean [headers=" + headers + ", contents=" + contents + "]";
    }
}
