REM If you see this:
REM 
REM "You need a passphrase to unlock the secret key for
REM user: "Support <support@coherentlogic.com>"
REM 2048-bit RSA key, ID ...DAC, created 2012-03-30"
REM
REM See also:
REM
REM 0.) https://central.sonatype.org/publish/publish-maven/
REM 1.) https://blog.sonatype.com/2010/11/what-to-do-when-nexus-returns-401/
REM 2.) https://stackoverflow.com/questions/25044403/mvn-deploy-maven-gpg-plugin1-5sign-failing-has-worked-for-a-week-now-no
REM
REM and try:
REM
REM mvn -Dgpg.passphrase=yourpassphrase deploy
REM
REM and also include the following otherwise the tests will fail:
REM
REM -Dgpg.passphrase=REDACTED -DFRED_API_KEY=REDACTED
REM
REM If you receive a 401 unauthorized then check the password in the settings.xml file and debug using the following example
REM
REM curl -v -u yourUsername:yourPassword https://oss.sonatype.org/service/local/staging/deploy/maven2/com/coherentlogic/.../data-model-core/.../data-model-core-3.0.3-SNAPSHOT.jar --request PUT --data ./data-model-core/target/data-model-core-3.0.3-SNAPSHOT.jar
REM
REM Or use: https://central.sonatype.org/publish/publish-maven/
REM
mvn clean source:jar javadoc:jar package gpg:sign deploy -P release
