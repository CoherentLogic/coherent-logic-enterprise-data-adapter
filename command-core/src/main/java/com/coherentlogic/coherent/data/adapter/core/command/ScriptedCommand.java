package com.coherentlogic.coherent.data.adapter.core.command;

import java.io.Reader;
import java.util.function.Function;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import com.coherentlogic.coherent.data.adapter.core.exceptions.ScriptRuntimeException;

public abstract class ScriptedCommand<X, Y> {//implements Function<X, Y> {

    public static final String GROOVY = "Groovy";

//    private final ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
//
    private final ScriptEngine scriptEngine; //= factory.getEngineByName("JavaScript");

//    engine.eval("print('Hello, World')");

//    public ScriptedCommand (String shortName) {
//        this (scriptEngineManager.getEngineByName(shortName));
//    }

    public ScriptedCommand (ScriptEngine scriptEngine) {
        this.scriptEngine = scriptEngine;
    }

    protected ScriptEngine getScriptEngine () {
        return scriptEngine;
    }

    public Y eval(String script, ScriptContext context) {
        try {
            return (Y) scriptEngine.eval(script, context);
        } catch (ScriptException scriptException) {
            throw new ScriptRuntimeException("The eval method failed for the script: " + script + " and context: "
                + context, scriptException);
        }
    }

    public Function<X, Y> applying (String script, ScriptContext context) {
        return new Function<X, Y> () {
            @Override
            public Y apply(X t) {
                return eval (script, context);
            }
        };
    }

    public Y eval(Reader reader, ScriptContext context) {
        try {
            return (Y) scriptEngine.eval(reader, context);
        } catch (ScriptException scriptException) {
            throw new ScriptRuntimeException("The eval method failed for the reader: " + reader + " and context: "
                + context, scriptException);
        }
    }

    public Y eval(String script) {
        try {
            return (Y) scriptEngine.eval(script);
        } catch (ScriptException scriptException) {
            throw new ScriptRuntimeException("The eval method failed for the script: " + script, scriptException);
        }
    }

    public Function<X, Y> applying (String script) {
        return new Function<X, Y> () {
            @Override
            public Y apply(X t) {
                return eval (script);
            }
        };
    }

    public Y eval(Reader reader) {
        try {
            return (Y) scriptEngine.eval(reader);
        } catch (ScriptException scriptException) {
            throw new ScriptRuntimeException("The eval method failed for the reader: " + reader, scriptException);
        }
    }

    public Y eval(String script, Bindings bindings) {
        try {
            return (Y) scriptEngine.eval(script, bindings);
        } catch (ScriptException scriptException) {
            throw new ScriptRuntimeException("The eval method failed for the script: " + script + " and bindings: "
                + bindings, scriptException);
        }
    }

    public Y eval(Reader reader, Bindings bindings) {
        try {
            return (Y) scriptEngine.eval(reader, bindings);
        } catch (ScriptException scriptException) {
            throw new ScriptRuntimeException("The eval method failed for the reader: " + reader + " and bindings: "
                + bindings, scriptException);
        }
    }
}
