package com.coherentlogic.coherent.data.adapter.core.command;

import java.util.function.Function;

/**
 * Works as follows:
 *
 * User creates an instance of this command and the pluginContainer will be assigned prior to it being executed.
 */
public abstract class PluginAwareCommand<X, Y> implements Function<X, Y> {

	Object getPluginContainer () {
		return null;
	}
}
