package com.coherentlogic.coherent.data.model.core.domain;

import java.io.Serializable;
import java.util.function.Consumer;

/**
 * 
 * @param <D> The type of data being consumed by the visitor.
 */
public interface VisitorSpecification<D> extends Serializable {

    void visit (Consumer<D> visitor);
}
