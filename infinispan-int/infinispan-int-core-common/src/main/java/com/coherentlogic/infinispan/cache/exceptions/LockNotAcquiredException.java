package com.coherentlogic.infinispan.cache.exceptions;

import org.springframework.core.NestedRuntimeException;

public class LockNotAcquiredException extends NestedRuntimeException {

    private static final long serialVersionUID = 2405723075368328799L;

    public LockNotAcquiredException(String message) {
        super(message);
    }
}
