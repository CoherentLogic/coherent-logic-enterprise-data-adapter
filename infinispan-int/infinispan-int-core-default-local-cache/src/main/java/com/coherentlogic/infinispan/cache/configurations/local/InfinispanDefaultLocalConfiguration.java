package com.coherentlogic.infinispan.cache.configurations.local;

import org.infinispan.configuration.cache.Configuration;
import org.infinispan.configuration.cache.ConfigurationBuilder;
import org.infinispan.configuration.global.GlobalConfiguration;
import org.infinispan.configuration.global.GlobalConfigurationBuilder;
import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.transaction.LockingMode;
import org.infinispan.transaction.TransactionMode;
import org.infinispan.transaction.tm.EmbeddedTransactionManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory;



@org.springframework.context.annotation.Configuration
public class InfinispanDefaultLocalConfiguration {

    public static final String DEFAULT_CACHE_NAME = "Coherent Logic Enterprise Data Adapter Default Local Cache";

    public static final String DEFAULT_JMX_DOMAIN = "Coherent Logic Enterprise Data Adapter Default JMX Domain";

    public static final String DEFAULT_INFINISPAN_CACHE_MANAGER_BEAN_NAME = "defaultInfinispanCacheManager",
        DEFAULT_INFINISPAN_CACHE_SERVICE_PROVIDER_FACTORY_BEAN_NAME = "defaultInfinispanCacheServiceProviderFactory";

    /**
     * Returns the equivalent of:
     * 
return new DefaultCacheManager (
            new ByteArrayInputStream (
                '''<infinispan
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                    xsi:schemaLocation="urn:infinispan:config:9.1 http://infinispan.org/schemas/infinispan-config-9.1.xsd"
                    xmlns="urn:infinispan:config:9.1">
                       <cache-container default-cache="localLEICache">
                           <local-cache name="localLEICache">
                               <transaction
                                transaction-manager-lookup="org.infinispan.transaction.lookup.GenericTransactionManagerLookup"
                                stop-timeout="30000"
                                auto-commit="true"
                                locking="PESSIMISTIC"
                                mode="FULL_XA"/>
                           </local-cache>
                       </cache-container>
                   </infinispan>'''.getBytes ()
            )
        )
     * @return
     * 
     * ----- These work and tests pass here but in Groovy it fails because it's looking for core related dependencies
     *       and NOT local and I'm not sure why.

        GlobalConfiguration globalConfiguration = globalConfigurationBuilder
            .nonClusteredDefault()
            .transport()
                .defaultTransport()
            .globalJmxStatistics()
                .allowDuplicateDomains(true)
                .jmxDomain(DEFAULT_JMX_DOMAIN)
                .enable()
            .build();

Configuration configuration = configurationBuilder
            .transaction()
            .transactionManagerLookup(new GenericTransactionManagerLookup ())
                .cacheStopTimeout(3000)
                .autoCommit(true)
                .lockingMode(LockingMode.PESSIMISTIC)
                .transactionMode(TransactionMode.TRANSACTIONAL)
            .build();

     * -----
     *
     */
    @Bean(name=DEFAULT_INFINISPAN_CACHE_MANAGER_BEAN_NAME)
    public EmbeddedCacheManager getCacheManager () {

        GlobalConfigurationBuilder globalConfigurationBuilder = new GlobalConfigurationBuilder ();

        GlobalConfiguration globalConfiguration = globalConfigurationBuilder
            .nonClusteredDefault()
            .defaultCacheName(DEFAULT_CACHE_NAME)
            .transport()
                .defaultTransport()
            .globalJmxStatistics()
                .allowDuplicateDomains(true)
                .jmxDomain(DEFAULT_JMX_DOMAIN)
                .enable()
            .build();

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder ();

//        Configuration configuration = configurationBuilder
//            .clustering()
//                .cacheMode(CacheMode.LOCAL)
//            .transaction()
//                .transactionMode(TransactionMode.TRANSACTIONAL)
//                .transactionManagerLookup(new GenericTransactionManagerLookup ())
//                .cacheStopTimeout(30000)
//                .autoCommit(true)
//                .lockingMode(LockingMode.PESSIMISTIC)
//                .recovery()
//                    .enabled(true)
//            .build();

// Just this works but is failing in Groovy.
//        Configuration configuration = configurationBuilder
//                .transaction()
//                    .transactionMode(TransactionMode.TRANSACTIONAL)
//                .build();

        Configuration configuration = configurationBuilder
            .transaction()
//            .transactionManagerLookup(
//                new EmbeddedTransactionManager ()
//            )
                .cacheStopTimeout(3000)
                .autoCommit(true)
                .lockingMode(LockingMode.PESSIMISTIC)
                .transactionMode(TransactionMode.TRANSACTIONAL)
        //  /* When true will result in an exception when running in the Groovy Console (only (?), to my knowledge):
        //   *
        //   *     ISPN000392: Recovery not supported with Synchronization
        //   */
        ////  .useSynchronization(true)
            .build();

        return new DefaultCacheManager (configuration);
    }
//  .transaction()
//  .transactionManagerLookup(new GenericTransactionManagerLookup ())
//  .cacheStopTimeout(3000)
//  .autoCommit(true)
//  .lockingMode(LockingMode.PESSIMISTIC)
//  .transactionMode(TransactionMode.TRANSACTIONAL)
//  /* When true will result in an exception when running in the Groovy Console (only (?), to my knowledge):
//   *
//   *     ISPN000392: Recovery not supported with Synchronization
//   */
////  .useSynchronization(true)
//  .recovery()
//      .enabled(true) // This will give us FULL_XA

    @Bean(name=DEFAULT_INFINISPAN_CACHE_SERVICE_PROVIDER_FACTORY_BEAN_NAME, initMethod="start", destroyMethod="stop")
    public DefaultInfinispanCacheServiceProviderFactory getDefaultInfinispanCacheServiceProviderFactory (
        @Qualifier(DEFAULT_INFINISPAN_CACHE_MANAGER_BEAN_NAME) EmbeddedCacheManager embeddedCacheManager) {

        return new DefaultInfinispanCacheServiceProviderFactory (embeddedCacheManager, DEFAULT_CACHE_NAME);
    }

    public static final String DEFAULT_LOCAL_INFINISPAN_CACHE_SERVICE_PROVIDER_BEAN_NAME =
        "defaultLocalInfinispanCacheServiceProvider";

    @Bean(name=DEFAULT_LOCAL_INFINISPAN_CACHE_SERVICE_PROVIDER_BEAN_NAME)
    public CacheServiceProviderSpecification<String> getDefaultInfinispanCacheServiceProvider (
        @Qualifier (DEFAULT_INFINISPAN_CACHE_SERVICE_PROVIDER_FACTORY_BEAN_NAME)
            DefaultInfinispanCacheServiceProviderFactory defaultInfinispanCacheServiceProviderFactory
    ) {
        return defaultInfinispanCacheServiceProviderFactory.getInstance();
    }
}
