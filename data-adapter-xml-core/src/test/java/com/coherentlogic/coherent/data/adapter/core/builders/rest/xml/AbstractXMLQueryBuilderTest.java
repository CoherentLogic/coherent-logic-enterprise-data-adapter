package com.coherentlogic.coherent.data.adapter.core.builders.rest.xml;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.coherentlogic.coherent.data.adapter.core.exceptions.ConversionFailedException;

public class AbstractXMLQueryBuilderTest {

    private static final String TEST = "test", TEST_COM = "http://www.test.com/";

    private ExampleXMLQueryBuilder queryBuilder = null;

    private String fredAPIKey = System.getProperty("FRED_API_KEY");

    @BeforeEach
    public void setUp() throws Exception {

        if (fredAPIKey == null)
            fredAPIKey = System.getenv("FRED_API_KEY");

        queryBuilder = new ExampleXMLQueryBuilder (null, TEST_COM);
    }

    @AfterEach
    public void tearDown() throws Exception {
        queryBuilder = null;
    }

    @Test
    public void doGetAsDocument () {

        Document document = queryBuilder.doGetAsXMLDocument(
            "<?xml version = '1.0' encoding = 'UTF-8' standalone = 'yes'?><foo bar='baz' id='fooId'>boo</foo>",
            (Document _document) -> { return _document;});

        assertNotNull(document);

        assertEquals ("baz", document.getDocumentElement().getAttribute("bar"));
    }

    @Test
    public void doGetAsDocumentUsingBadXML () {

        assertThrows(ConversionFailedException.class, () -> {
            Document document = queryBuilder.doGetAsXMLDocument(
                "<==--! <<",
                (Document _document) -> { return _document;});
            }
        );
    }

    /**
     * Required: VM Args: or command line: -DFRED_API_KEY=...
     */
    @Test
    public void testDoGetAsXMLDocumentAndTraverse () throws IOException {

        final ExampleXMLQueryBuilder testQueryBuilder = new ExampleXMLQueryBuilder(
            new RestTemplate (), "https://api.stlouisfed.org/fred/series/observations");

        final AtomicInteger ctr = new AtomicInteger(0);

        testQueryBuilder
            .addParameter("api_key", fredAPIKey)
            .addParameter("series_id", "EFFR")
            .doGetAsXMLDocumentAndTraverse (
                (Node node) -> {
                    return ctr.incrementAndGet();
                }
            );

        assertTrue(9700 < ctr.get());
    }

    /**
     * See here: https://stackoverflow.com/questions/21413978/convert-an-xml-file-to-csv-file-using-java
     *
     * @throws TransformerFactoryConfigurationError 
     * @throws TransformerConfigurationException 
     */
    @Test
    public void testDoGetAsXMLDocumentAndTransform () throws TransformerConfigurationException, TransformerFactoryConfigurationError {

        final ExampleXMLQueryBuilder testQueryBuilder = new ExampleXMLQueryBuilder(
            new RestTemplate (), "https://api.stlouisfed.org/fred/series/observations");

        InputStream stylesheetInputStream = this.getClass().getClassLoader().getResourceAsStream("fred-observations-stylesheet.xsl");

        StreamSource stylesheetStreamSource = new StreamSource(stylesheetInputStream);

        final Transformer transformer = TransformerFactory.newInstance().newTransformer(stylesheetStreamSource);

        String result = testQueryBuilder
            .addParameter("api_key", fredAPIKey)
            .addParameter("series_id", "EFFR")
            .doGetAsXMLDocumentAndTransform(
                transformer,
                (Node node, Transformer innerTransformer) -> {

                    Source source = new DOMSource(node);

                    Writer writer = new StringWriter();

                    Result innerResult = new StreamResult(writer);

                    try {
                        transformer.transform(source, innerResult);
                    } catch (TransformerException transformerException) {
                        throw new RuntimeException(
                            "Unable to transform the source to the result.", transformerException);
                    }

                    return writer.toString();
                }
            );

        // System.out.println ("=====> result: " + result + ", =====> escUri: " + testQueryBuilder.getEscapedURI());
        
        // Note that if the FRED data changes then this might break so if the test fails here, make sure that the string
        // we're testing with is, in fact, in the results.
        assertTrue(result.contains(",2.41"));
    }
}
