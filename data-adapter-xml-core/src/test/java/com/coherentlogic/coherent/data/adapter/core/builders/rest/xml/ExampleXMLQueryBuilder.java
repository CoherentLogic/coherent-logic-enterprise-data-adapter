package com.coherentlogic.coherent.data.adapter.core.builders.rest.xml;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.command.CommandExecutorSpecification;

public class ExampleXMLQueryBuilder extends AbstractXMLQueryBuilder<String> {

    public static final String FOO = "foo", BAR = "bar", BAZ = "baz";

    protected ExampleXMLQueryBuilder(RestTemplate restTemplate, String uri) {
        super (restTemplate, uri);
    }

    public ExampleXMLQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache,
        CommandExecutorSpecification<String> commandExecutor
    ) {
        super(restTemplate, uri, cache, commandExecutor);
    }

    protected ExampleXMLQueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    /**
     * Extends the path to include 'foo' -- for example:
     *
     * http://www.foobar.zzz/foo/
     */
    public ExampleXMLQueryBuilder foo () {

        extendPathWith(FOO);

        return this;
    }

    /**
     * Setter method for the bar parameter.
     */
    public ExampleXMLQueryBuilder setBar (String bar) {

        addParameter(BAR, bar);

        return this;
    }

    @Override
    protected ExampleXMLQueryBuilder addParameter(String name, Object value) {
        return (ExampleXMLQueryBuilder) super.addParameter(name, value);
    }

    @Override
    public String getCacheKey() {
        return getEscapedURI();
    }

    @Override
    protected <T> T doExecute(Class<T> type) {
        return null;
    }
}
